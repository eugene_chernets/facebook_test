package com.chernets.testignite.data;

/**
 * Created by evgeniych on 16.03.2016.
 */
public interface Const {
    String PERMISSION_PUBLIC_PROFILE = "public_profile";
    String PERMISSION_USER_FRIENDS = "user_friends";
}
