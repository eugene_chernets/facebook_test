package com.chernets.testignite.adapters;

import android.content.Context;
import android.widget.Toast;

import com.chernets.testignite.R;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

/**
 * Created by evgeniych on 16.03.2016.
 */
public class FacebookCallbackAdapter implements FacebookCallback<LoginResult> {

    private Context context;

    public FacebookCallbackAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
    }
    @Override
    public void onCancel() {
        Toast.makeText(context, context.getString(R.string.cancelled), Toast.LENGTH_LONG).show();
    }
    @Override
    public void onError(FacebookException exception) {
        Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

}
