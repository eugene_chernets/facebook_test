package com.chernets.testignite.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by evgeniych on 14.03.2016.
 */
public class Friend {

    private final static String TAG = Friend.class.getSimpleName();

    private FriendUpdatedListener listener;
    private String id;
    private String firstName;
    private String lastName;
    private Bitmap photo;

    public Friend(String id, String firstName, String lastName, Bitmap photo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
    }

    public Friend(final FriendUpdatedListener listener, JSONObject object) {
        Log.d(TAG, "Friend: " + object.toString());
        try {
            this.id = object.getString("id");
            this.firstName = object.getString("first_name");
            this.lastName = object.getString("last_name");
            new AsyncTask<URL, Bitmap, Bitmap>() {
                @Override
                protected Bitmap doInBackground(URL... params) {
                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory.decodeStream(params[0].openConnection().getInputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return bitmap;
                }
                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    photo = bitmap;
                    listener.update();
                }
            }.execute(new URL("https://graph.facebook.com/" + this.id + "/picture?type=small"));
        } catch (JSONException e) {
            this.id = "error";
            this.firstName = "first_name";
            this.lastName = "last_name";
            this.photo = null;
            Log.d(TAG, "Friend: JSONException" + e.getMessage());
        } catch (MalformedURLException e) {
            Log.d(TAG, "Friend: MalformedURLException" + e.getMessage());
            this.photo = null;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public interface FriendUpdatedListener {
        void update();
    }
}
