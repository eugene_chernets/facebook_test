package com.chernets.testignite;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chernets.testignite.adapters.FacebookCallbackAdapter;
import com.chernets.testignite.adapters.FriendsListAdapter;
import com.chernets.testignite.data.Const;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by evgeniych on 14.03.2016.
 */
public class ChatHistoryActivity extends Activity {

    private final static String TAG = ChatHistoryActivity.class.getSimpleName();

    private final static String graphPath = "/me/inbox";

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallbackAdapter(this) {
            @Override
            public void onSuccess(LoginResult loginResult) {
                requestChatHistory();
            }
        });

        if (AccessToken.getCurrentAccessToken() != null) {
            Log.d(TAG, "AccessToken contains permission");
            requestChatHistory();
        } else {
            Log.d(TAG, "trying to login with needed permission");
            LoginManager.getInstance().logInWithReadPermissions(this,
                    Arrays.asList(Const.PERMISSION_PUBLIC_PROFILE, Const.PERMISSION_USER_FRIENDS));
        }

        setContentView(R.layout.activity_chat_history);
        findViewById(R.id.history_btn).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.caption)).setText(getString(R.string.title_activity_chat_history));

    }

    private void requestChatHistory() {
        Log.d(TAG, "requestChatHistory");
        GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(),
                graphPath, new GraphRequest.Callback() {

                    @Override
                    public void onCompleted(GraphResponse response) {
                        FacebookRequestError fbrErr = response.getError();
                        if (fbrErr != null)
                            Log.d(TAG, "onCompleted ErrorMessage: " + fbrErr.getErrorMessage());

                        Log.d(TAG, "onCompleted: " + response.toString());

//                        if (objects != null)
//                            friendsList.setAdapter(new FriendsListAdapter(
//                                    FriendsListActivity.this, getFriendsListFromJSONArray(objects)));
//                        else
//                            Log.d(TAG, "response.getJSONArray() == null");
                    }
                }

        );
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,first_name,last_name");
//        request.setParameters(parameters);
        request.executeAsync();
    }
}
