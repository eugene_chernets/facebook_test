package com.chernets.testignite.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chernets.testignite.R;
import com.chernets.testignite.data.Friend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by evgeniych on 14.03.2016.
 */
public class FriendsListAdapter extends BaseAdapter {
    private static final String TAG = FriendsListAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<Friend> list;
    private ArrayList<Friend> workList;

    public FriendsListAdapter(Context context, ArrayList<Friend> list) {
        this.context = context;
        this.list = list;
        this.workList = (ArrayList) list.clone();
    }

    @Override
    public int getCount() {
        return workList.size();
    }

    @Override
    public Object getItem(int position) {
        return workList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.friends_list_item, null);
        Friend friend = workList.get(position);
        ((TextView) view.findViewById(R.id.name)).setText(friend.getFirstName() + " " + friend.getLastName());

        if (friend.getPhoto() != null)
            ((ImageView) view.findViewById(R.id.photo)).setImageBitmap(friend.getPhoto());

        return view;
    }

    public void setSorting(SortingType sorting) {
        Collections.sort(workList, sorting == SortingType.FIRST_NAME_ASCENDING
                ? new FirstNameComparator() : new LastNameComparator());
        notifyDataSetChanged();
    }

    public void setFilter(String filter) {
        if (filter.isEmpty()) {
            workList = (ArrayList) list.clone();
        } else {
            workList.clear();
            for (Friend friend : list) {
                if (friend.getFirstName().toLowerCase().contains(filter.toLowerCase()) ||
                        friend.getLastName().toLowerCase().contains(filter.toLowerCase()))
                    workList.add(friend);
            }
        }
        notifyDataSetChanged();
    }

    public enum SortingType {
        FIRST_NAME_ASCENDING,
        LAST_NAME_ASCENDING
    }

    private class FirstNameComparator implements Comparator<Friend> {

        @Override
        public int compare(Friend lhs, Friend rhs) {
            return lhs.getFirstName().compareToIgnoreCase(rhs.getFirstName());
        }
    }

    private class LastNameComparator implements Comparator<Friend> {

        @Override
        public int compare(Friend lhs, Friend rhs) {
            return lhs.getLastName().compareToIgnoreCase(rhs.getLastName());
        }
    }

}
