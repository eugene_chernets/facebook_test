package com.chernets.testignite;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.chernets.testignite.adapters.FacebookCallbackAdapter;
import com.chernets.testignite.adapters.FriendsListAdapter;
import com.chernets.testignite.data.Const;
import com.chernets.testignite.data.Friend;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by evgeniych on 10.03.2016.
 */
public class FriendsListActivity extends Activity implements View.OnClickListener, Friend.FriendUpdatedListener {

    private final static String TAG = FriendsListActivity.class.getSimpleName();

    private CallbackManager callbackManager;
    private ListView friendsList;
    private FriendsListAdapter friendsListAdapter;
    private Spinner sorterSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallbackAdapter(this) {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getRecentlyGrantedPermissions().contains(Const.PERMISSION_USER_FRIENDS)) {
                    Log.d(TAG, "onSuccess permission granted");
                    requestFriendsList();
                }
            }
        });

        AccessToken token = AccessToken.getCurrentAccessToken();

        if (token != null && token.getPermissions().contains(Const.PERMISSION_USER_FRIENDS)) {
            requestFriendsList();
        } else {
            LoginManager.getInstance().logInWithReadPermissions(this,
                    Arrays.asList(Const.PERMISSION_PUBLIC_PROFILE, Const.PERMISSION_USER_FRIENDS));
        }

        setContentView(R.layout.activity_friends);
        friendsList = (ListView) findViewById(R.id.friends_list);

        sorterSpinner = (Spinner) findViewById(R.id.sorter_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource
                (this, R.array.sorter_items, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sorterSpinner.setAdapter(spinnerAdapter);
        sorterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (friendsListAdapter != null)
                    switch (position) {
                        case 0:
                            friendsListAdapter.setSorting(FriendsListAdapter.SortingType.FIRST_NAME_ASCENDING);
                            break;
                        case 1:
                            friendsListAdapter.setSorting(FriendsListAdapter.SortingType.LAST_NAME_ASCENDING);
                            break;
                    }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        findViewById(R.id.history_btn).setOnClickListener(this);
        ((TextView) findViewById(R.id.caption)).setText(getString(R.string.title_activity_friends));

        final EditText filter = (EditText) findViewById(R.id.filter_text);
        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (friendsListAdapter != null)
                    friendsListAdapter.setFilter(s.toString());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void requestFriendsList() {
        GraphRequest request = GraphRequest.newMyFriendsRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONArrayCallback() {
                    @Override
                    public void onCompleted(JSONArray objects, GraphResponse response) {
                        FacebookRequestError fbrErr = response.getError();
                        if (fbrErr != null)
                            Log.d(TAG, "onCompleted ErrorMessage: " + fbrErr.getErrorMessage());

                        Log.d(TAG, "onCompleted: " + response.toString());

                        if (objects != null) {
                            friendsListAdapter = new FriendsListAdapter(
                                    FriendsListActivity.this, getFriendsListFromJSONArray(objects));
                            friendsList.setAdapter(friendsListAdapter);
                            friendsListAdapter.setSorting(FriendsListAdapter.SortingType.LAST_NAME_ASCENDING);
                            sorterSpinner.setSelection(1);
                        } else {
                            Log.d(TAG, "response.getJSONArray() == null");
                        }
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private ArrayList<Friend> getFriendsListFromJSONArray(JSONArray array) {
        ArrayList<Friend> friends = new ArrayList<>();
        Log.d(TAG, "getFriendsListFromJSONArray: " + array.toString());
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                friends.add(new Friend(this, object));
            }
        } catch (JSONException e) {
            if (friends.size() == 0)
                friends.add(new Friend("id", "Name", "Surname",
                        BitmapFactory.decodeResource(getResources(), android.R.drawable.sym_def_app_icon)));
            Log.d(TAG, "getFriendsListFromJSONArray exception: " + e.getMessage());
            e.printStackTrace();
        }
        return friends;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.history_btn:
                Intent intent = new Intent(FriendsListActivity.this, ChatHistoryActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void update() {
        if (friendsListAdapter != null)
            friendsListAdapter.notifyDataSetChanged();
    }
}
